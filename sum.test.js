const {describe, expect, test} = require('@jest/globals');
const  sum = require("./sum.js");

describe('Sum function', () => {

  test('Sum is work 4', () => {
    expect(sum(2, 2)).toBe(4)
  })

  test('Sum is work 8', () => {
    expect(sum(3, 5)).toBe(8)
  })
});
